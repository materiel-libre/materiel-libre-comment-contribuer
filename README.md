Fichier vide pour l'instant. Voir les progressions sur la branche `develop` et sur les branches `PROPOSITION/...`
=======
# materiel-libre-comment-contribuer

Instance git sur laquelle se construisent et se versionnent, les informations concernant la façon dont il est possible de contribuer aux projets du groupe `materiel-libre` sur framagit.

*   [Worflow simplifié utilisé pour les dépôt du groupe materiel-libre](GUIDE_INSTRUCTIONS_GIT/0-guide-workflow-git-materiel-libre.md)
