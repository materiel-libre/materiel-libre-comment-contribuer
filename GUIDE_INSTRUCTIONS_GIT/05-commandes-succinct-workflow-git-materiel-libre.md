---
author: antoine c
description: |
    Projet de rédaction d'instructions git 
    pour les dépôts du groupe materiel-libre
title: LIGNES DE COMMANDES GIT NON DÉTAILLÉES / WORKFLOW GIT SIMPLIFIÉ UTILISÉ POUR LES DÉPÔTS MATERIEL-LIBRE
---

# WORKFLOW EN LIGNE DE COMMANDE / WORKFLOW SIMPLIFIÉ UTILISÉ POUR LES DÉPÔTS MATERIEL-LIBRE

## Dans cette page:

*   [Avant toute chose](#avant-toute-chose) 
*   [Opérations préalables](#opérations-préalables)
*   [Enchaînement du workflow en lignes de commandes](#enchaînement-du-workflow-en-lignes-de-commandes)
    *   [Mettre à jour le dépôt local](#mettre-à-jour-le-dépôt-local)
    *   [Créer une proposition](#créer-une-proposition)
    *   [Travaillez sur votre proposition](#travaillez-sur-votre-proposition)
    *   [Prépararer la fusion](#prépararer-la-fusion)
    *   [Fusionner](#fusionner)
    *   [Versionner](#versionner)
    *   [Corriger des versions publiées](#corriger-des-versions-publiées)

## Avant toute chose

SVP, NE FAITES PAS L'IMPASSE sur la lecture des 3 documents suivants:

*   Le [Guide du workflow simplifié utilisé pour le groupe materiel-libre](00-guide-workflow-git-materiel-libre.md)
*   Les [principes généraux du workflow simplifié utilisé pour le groupe materiel-libre](03-principes-workflow-git-materiel-libre.md)
*   La [convention des noms des branches pour le groupe materiel-libre](04-conventions-workflow-git-materiel-libre.md)

## Opérations préalables

*   [Création d'un dépôt materiel-libre](01-creation-depot-workflow-git-materiel-libre.md)
*   [Réglages à effectuer sur le dépôt](02-reglages-master-develop-workflow-git-materiel-libre.md)

## Enchaînement du workflow en lignes de commandes

### Mettre à jour le dépôt local
```
    git checkout master
    git pull origin master
    git status
    git checkout develop
    git pull origin develop
    git status
```
### Créer une proposition

Prendre connaissance de la [convention des noms des branches](04-conventions-workflow-git-materiel-libre.md)
```
    git checkout develop
    git branch -a
    git branch PROPOSITION/Nom-De-La-Proposition
    git checkout PROPOSITION/Nom-De-La-Proposition
    git push -u origin PROPOSITION/Nom-De-La-Proposition
``` 

### Travaillez sur votre proposition 

#### Produire le contenu de la proposition en local

Produisez vos contenus, ajoutez, supprimez, modifiez, etc ...

#### Enregistrer les changements en local
```
    git checkout PROPOSITION/Nom-De-La-Proposition
    git add -A .
    git commit -m "commentaire descriptif"
``` 

#### Régulièrement, rembobiner

Attention: bien écrire la commande rebase en plaçant un "/" devant la branche concernée par le rembobinage. Sinon, vous aurez des erreurs.
```
    git checkout PROPOSITION/Nom-De-La-Proposition
    git fetch origin
    git rebase origin/master
    git rebase origin/develop
    git rebase origin/PROPOSITION/Nom-De-La-Proposition
```
Si conflits, résolvez !

#### Partager sur le site distant pour favoriser le travail collaboratif et obtenir des avis:

Plus c'est partagé tôt, mieux c'est ! Cela permet aux autres de savoir ce que vous faites. Cela peut leur donner des idées d'améliorations à partager avec vous.

```
    git checkout PROPOSITION/Nom-De-La-Proposition
    git push -u origin PROPOSITION/Nom-De-La-Proposition
``` 

#### Répéter autant de fois que nécessaire

Répétez les étapes du workflow de production de votre proposition, autant de fois que vous le souhaitez, tout au long de la progression de vos travaux de production.

### Prépararer la fusion

#### Faire un dernier rembobinage:

Attention: bien écrire la commande rebase en plaçant un "/" devant la branche concernée par le rembobinage. Sinon, vous aurez des erreurs.
```
    git checkout PROPOSITION/Nom-De-La-Proposition
    git fetch origin
    git rebase origin/master
    git rebase origin/develop
    git rebase origin/PROPOSITION/Nom-De-La-Proposition
```
Si conflits, résolvez !

#### Demander une autorisation de fusion, une fois le travail accompli !

```
    git checkout PROPOSITION/Nom-De-La-Proposition
    git push -o merge_request.create -o merge_request.target=PROPOSITION/Nom-De-La-Proposition
```

#### Effectuer les modifications demandées

S'il vous est demandé de faire des modifications, re-travaillez votre proposition, effectuez les éventuelles modifications demandées, recommencez les étapes relatives à la production de la proposition, puis rembobinez, puis redemandez une autorisation de fusionner votre branche dans `develop`. Recommencez ces opérations tant que l'autorisation de fusion n'est pas obtenue. Une fois que tout va bien, passez à l'étape d'après.

### Fusionner

une fois l'autorisation de fusion obtenue

**A T T E N T I O N - D A N G E R !!!**

Ne faites pas ce qui suit si vous avez l'intention de revenir sur votre branche pour continuer des travaux. Si vous voulez continuer à travailler sur votre branche, continuez à répéter les instructions précédentes. **Sinon, vous allez au devant de grandes difficultés !!!**

```
    git checkout PROPOSITION/Nom-De-La-Proposition
    git rebase -i develop
    git checkout develop
    git pull origin develop
    git merge --no-ff PROPOSITION/Nom-De-La-Proposition
    git push origin develop
    git branch -d PROPOSITION/Nom-De-La-Proposition
    git push origin --delete PROPOSITION/Nom-De-La-Proposition
    
```

### Versionner

#### Choisir un numéro de version

Informez-vous sur la [convention de numéros des versions](04-conventions-workflow-git-materiel-libre.md).

Puis, regardez dans la liste des versions existantes:
```
    git checkout develop
    git tag
```
Décidez d'un nouveau numéro de version, arrivant à la suite des précédents selon la liste [NumVersion].

#### Repérer le numéro abbrégé du commit pour la version

```
    git checkout develop
    git log --abbrev-commit
```
Décidez du numéro de commit sur lequel versionner [NumCommit]

#### Procéder au versionnage

```
    git checkout develop
    git branch VERSIONNAGE/NumVersion NumCommit
    git checkout VERSIONNAGE/NumVersion
    git tag NumVersion
    git checkout develop
    git merge VERSIONNAGE/NumVersion
    git push --tags origin develop
    git branch -d VERSIONNAGE/NumVersion
    git checkout master
    git merge --f-only NumVersion
```

### Corriger des versions publiées

Utilisez ces instructions de "correctifs", uniquement pour appliquer des correctifs rapides à une version publiée (notion de rustine, de patch, de hot-fix). Si vous envisagez de produire d'importantes modifications, alors ce ne sont pas des correctifs, mains plutôt de nouveaux travaux de production d'une nouvelle version. Dans ce cas, il vous faut vous référer aux sections précédentes.

#### Déterminer correctement le numéro de correctif à appliquer

Repérez le numéro de la version sur laquelle vous allez opérer un correctif:
```
    git checkout master
    git tag
```
Le numéro de correctif [NumCorrectif], que vous allez appliquer, est le numéro de version à corriger augmenté d'un cran supérieur (exemple: 2.1.0 -> 2.1.1; 0.1.pre-alpha.1 -> 0.1.pre-alpha.2)

#### Créer la branche du correctif à produire
```
    git branch CORRECTIF/NumCorrectif master
    git checkout CORRECTIF/NumCorrectif
```

#### Appliquer le correctif

```
    git checkout CORRECTIF/NumCorrectif
    git tag NumCorrectif
    git checkout develop
    git merge CORRECTIF/NumCorrectif
    git push --tags origin develop
    git branch -d CORRECTIF/NumCorrectif
    git checkout master
    git merge --ff-only NumCorrectif
    
```

## Voir aussi

*   [Conventions pour nommer les branches](04-conventions-workflow-git-materiel-libre.md)

## Sources, inspirations, remerciements:

*   [le workflow "oneflow" d'endoflineblog.com](https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow)
*   [le workflow "simple git workflow d'Atlasian.com"](https://www.atlassian.com/blog/git/simple-git-workflow-simple)
