---
author: antoine c
description: |
    Projet de rédaction d'instructions git 
    pour les dépôts du groupe materiel-libre
title: WORKFLOW GIT SIMPLIFIÉ UTILISÉ POUR LES DÉPÔTS MATERIEL-LIBRE /  CONVENTION NOMS DES BRANCHES
---

## Nom des deux branches éternelles

  * `master` 
  * `develop` 

## Nom des branches éphémères 

### Principe général pour les noms des branches éphémères:

  * "`NOM-CATEGORIE-DE-LA-BRANCHE`" (+) "`/`" (+) "`Nom-De-La-Branche`"

"`NOM-CATEGORIE-DE-LA-BRANCHE`" en majuscule dont les mots sont séparés par des tirets du 6 ("`-`"), suivi du séparateur "`/`", suivi du "`Nom-De-La-Branche`" en minuscule dont les mots sont séparés par des tirets du 6 ("`-`").

### Les 3 uniques catégories de branches éphémères:

  * **PROPOSITIONS** # branche éphémère pour proposer des améliorations
  * **VERSIONNAGE** # branche éphémère pour préparer une version
  * **CORRECTIF** # branche éphémère pour effectuer une correction sur une version

### Exemple de noms de branches éphémères:

  * PROPOSITION/ma_jolie_proposition
  * VERSIONNAGE/3.0.beta.2
  * CORRECTIF/3.0.beta.3
  * PROJET/10_caracteristiques
  
## Numéro des versions

*   [MAJEUR].[MINEUR].[CORRECTIF]-[phases].[Numéro de phase]
*   [MAJEUR]= 0 => pré-versions d'essais, de dev, de brouillon, etc ...
*   [phases]: pre-alpha < alpha < beta < rc
*   Hiérarchie: 1.0.0-pre-alpha < 1.0.0-pre-alpha.1 < 1.0.0-alpha < 1.0.0-alpha.1 < 1.0.0-alpha.beta < 1.0.0-beta < 1.0.0-beta.2 < 1.0.0-beta.11 < 1.0.0-rc.1 < 1.0.0.

## Voir aussi:

Workflow simplifié en ligne de commandes:

*   [Descriptif non détaillé](workflow-resume-cmd.md)
*   [Descriptif détaillé](workflow-details-cmd.md)

Workflow simplifié via l'interface framagit:
*   [Descriptif non détaillé](workflow-resume-interface-gitlab.md)
*   [Descriptif détaillé](workflow-details-interface-gitlab.md)

Numéros de versions:
*   [Gestion sémantique des versions](https://semver.org/lang/fr/)
