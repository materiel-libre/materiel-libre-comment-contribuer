---
author: antoine c
description: |
    Projet de rédaction d'instructions git 
    pour les dépôts du groupe materiel-libre
title: WORKFLOW GIT SIMPLIFIÉ UTILISÉ POUR LES DÉPÔTS MATERIEL-LIBRE / PRINCIPES GÉNÉRAUX
---



## Principes généraux du Worflow simplifié utilisé pour les dépôt du groupe materiel-libre

Le workflow simplifié, met en jeu:

*   deux branches éternelles: `master` et `develop`
*   trois types de branches éphémères: `PROPOSITION/`; `VERSIONNAGE/`; `CORRECTIF/`

**Branche éternelle / significations:** une branche éternelle, est créée au moment de la création du dépôt. Elle est ensuite conservée pendant toute la vie du dépôt. Dans le cas des dépôts du groupe materiel-libre, leurs branches `master` et `develop` sont éternelles.

**Branche éphémère / significations:** les branches éphémères, sont créées au cours de la vie du dépôt, pour être ensuite fusionnées dans l'une des branches éternelles, puis détruites. Dans le  cas du groupe materiel-libre, ce sont les branches de production de propositions, de production de contenus de versions, et de productions de correctifs.

### Crocquis descriptif

[ici mettre une image du workflow]

### Descriptif des deux branches éternelles `master` et `develop`:

#### Branche `master`

La branche `master` sert à réceptionner les travaux versionnables. Sur la branche `master` se trouvent uniquement des contenus de version. Les autres contenus qui ne font pas partie de versions, ne sont pas sur la branche `master`. La branche `develop` part de la branche `master` mais n'y revient pas. Les branches de versionnages, partent de `develop` et arrivent dans `master`.

Caratéristiques:

*   éternelle
*   protégée (confère la page création dépôt distant)
*   

Branchements:

*   `develop` est créé sur la branche master: dès sa naissance, `develop` part de `master`
*   les branches de versionnages (`VERSIONNAGE/NomDeLaVersion`) partent de `develop` puis sont fusionnées sur `master`

Les deux branches, protégées, surlesquelles on n'écrit pas directement

*   `master` = une branche éternelle, protégée, avec du contenu "stable" versionné
*   `develop` = une branche éternelle avec du contenu non-stable, en développement, de travaux en cours, sans version, qui part de `master`, mais qui n'y revient pas (elle ne se fusionne pas dans `master`)

### Des branches éphémères, sur lesquelles sont effectuées les améliorations:

*   `PROPOSITION/nom_de_la_branche` = des branches éphémères, pour réaliser des propositions, des travaux d'améliorations, qui partent de la branche `develop` et reviennent ensuite se fusionner sur la branche `develop`
*   `VERSIONNAGE/ID_version` = des branches éphémères avec du contenu versionnable qui partent de la branche `develop` pour effectuer des versions sur la branche `master`, et qui fusionnent dans la branche `master`
*   `CORRECTIF/ID_Correctif` = des branches éphémères avec des correctifs rapides à apporter aux versions publiées sur la branche `master`, qui partent de la branche `master`, poussent le correctif dans `develop` puis fusionnent dans la branche `master`.


## Voir aussi

*   [Conventions pour nommer les branches](workflow-conventions-noms-branches.md)

## Sources, inspirations, remerciements:

*   https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow
*   https://www.atlassian.com/blog/git/simple-git-workflow-simple
