---
author: antoine c
description: |
    Projet de rédaction d'instructions git 
    pour les dépôts du groupe materiel-libre
title: CRÉATION D'UN DÉPÔT
---

[Retour vers le guide général d'instructions-git](guide-workflow-git.md)

## Sur cette page (contenu):

*   [Créer le dépôt sur Framagit](#Créer le dépôt sur Framagit)


## Créer un dépôt 

### Créer le dépôt sur Framagit

*   Se connecter sur Framagit
*   Aller sur la page du groupe [materiel-libre](https://framagit.org/materiel-libre)
*   Cliquer sur le bouton "New Project"
*   Nommer le projet en commençant par `materiel-libre` et en utilisant les tirets du 6 *"-"* entre chaque mot (exemple: materiel-libre-nom-du-projet)
*   Décrire le projet
*   Choisir la visibilité (normalement, c'est "public")
*   Cocher la case "Initialize repositary with a README"


### Créer le dépôt localement pour pouvoir travailler localement sur le dépôt et envoyer les travaux sur le dépôt distant

Définir un répertoire sur l'ordinateur où placer les travaux relatifs au dépôt

*   exemple: home -> GIT

```
    # se placer dans le répertoire choisi pour git en local
    $ cd Nom_Du_Repertoire_Git
    
    # Initialiser git
    $ git init
    
    # Cloner le dépôt distant créé chez framagit, sur l'ordinateur en local
    $ git clone adresse.ssh.a.copier.coller
```

