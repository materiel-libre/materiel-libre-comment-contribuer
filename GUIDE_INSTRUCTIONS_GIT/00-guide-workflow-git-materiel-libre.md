---
author: antoine c
description: |
    Projet de rédaction d'instructions git 
    pour les dépôts du groupe materiel-libre
title: GIT / WORKFLOW SIMPLIFIÉ UTILISÉ
---

## Pour les personnes qui préfèrent les lignes de commandes:

*   [Descriptif détaillé](05-commandes-succinct-workflow-git-materiel-libre.md) - workflow en lignes de commandes / descriptif succinct
*   [Descriptif succinct](06-commandes-details-workflow-git-materiel-libre.md) - workflow en lignes de commandes / descriptif détaillé

## Pour les personnes qui préfèrent utiliser l'interface framagit (instance gitlab chez framasoft)

*   [Descriptif détaillé](07-interface-gitlab-succinct-workflow-git-materiel-libre.md) - workflow via interface framagit / descriptif succinct
*   [Descriptif succinct](08-interface-gitlab-details-workflow-git-materiel-libre.md) - workflow via interface framagit / descriptif détaillé


----

//// Dessous, brouillons à déplacer ////

----

[FIXME !!]

## Worflow simplifié utilisé pour les dépôt du groupe materiel-libre

*   [Principes retenus](principes-workflow-git-simplifie.md)
*   Préalables: [création de dépôt](creation-depot.md) puis [réglages des branches master et develop](reglages-fichiers-master-develop.md)
*   [FIXME]

### Créer un dépôt distant sur Framagit puis le cloner localement sur un ordinateur

#### 1. Créer le dépôt sur Framagit

*   Se connecter sur Framagit
*   Aller sur la page du groupe [materiel-libre](https://framagit.org/materiel-libre)
*   Cliquer sur le bouton "New Project"
*   Nommer le projet en commençant par `materiel-libre` et en utilisant les tirets du 6 *"-"* entre chaque mot (exemple: materiel-libre-nom-du-projet)
*   Décrire le projet
*   Choisir la visibilité (normalement, c'est "public")
*   Cocher la case "Initialize repositary with a README"


#### 2. Créer le dépôt localement pour pouvoir travailler localement sur le dépôt et envoyer les travaux sur le dépôt distant

Définir un répertoire sur l'ordinateur où placer les travaux relatifs au dépôt

*   exemple: home -> GIT

```
    # se placer dans le répertoire choisi pour git en local
    $ cd Nom_Du_Repertoire_Git
    
    # Initialiser git
    $ git init
    
    # Cloner le dépôt distant créé chez framagit, sur l'ordinateur en local
    $ git clone adresse.ssh.a.copier.coller
```



## Sources, inspirations, remerciements:

*   https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow
*   https://www.atlassian.com/blog/git/simple-git-workflow-simple
