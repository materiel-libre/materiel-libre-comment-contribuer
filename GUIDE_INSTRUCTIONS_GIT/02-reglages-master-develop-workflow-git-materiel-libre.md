---
author: antoine c
description: |
    Projet de rédaction d'instructions git pour les dépôts du groupe materiel-libre sous la logique de "workflow simple": une branche stable éternelle (master); une branche éternelle de progression non-stable (develop); des branches éphémères de travail qui alimentent la branche develop.
title: RÉGLAGE DES BRANCHES MASTER ET DEVELOP
---

[Retour vers guide d'instruction git](guide-workflow-git.md)

## Réglages préalables des branches master et develop

**Créer la branche "develop"**

Se placer dans le répertoire du dépôt git créé précédement.

```
$ git checkout master
$ git checkout -b develop
$ git push -u origin develop

```

**Protéger la branche "develop" et la définir par défaut**

Aller sur le site framagit, se connecter, aller sur le projet, puis:

*   -> settings -> repository
*   branche par défaut: develop
*   protected branch: develop
    *   merge: maintainer 
    *   push: maintainer + developper

[Retour vers guide d'instruction git](guide-workflow-git.md)
